package com.revdebug.logerror;

import org.apache.log4j.Logger;

import java.io.*;

public class LogErrorTest {
    static Logger log = Logger.getLogger(LogErrorTest.class.getName());

    public static void main(String[] args) {
        logStringExample();
        logStringExceptionExample();
    }

    private static void logStringExample() {
        File file = new File("totallyNonExistentFile");
        if (!file.exists()) {
            log.error("The file does not exist");
        }
    }

    private static void logStringExceptionExample() {
        try {
            int divisionByZeroResult = 1 / 0;
        } catch (ArithmeticException e) {
            log.error("Cant divide by 0", e);
        }
    }
}
